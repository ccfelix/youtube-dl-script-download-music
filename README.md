Dependências desse arquivo bat:

youtube-dl.exe

Link: https://ytdl-org.github.io/youtube-dl/

Arquivos do ffmpeg:

ffmpeg.exe
ffplay.exe
ffprobe.exe

Link: https://ffmpeg.zeranoe.com/builds/

Todos os executáveis devem estar no path (variáveis de ambiente) do sistema.
