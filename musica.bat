@echo off
c:
cd /
if not exist "%USERPROFILE%\Desktop\Musicas\" mkdir "%USERPROFILE%\Desktop\Musicas"
cd "%USERPROFILE%\Desktop\Musicas"
set /p music=Insira o link da musica ou lista (YouTube):
youtube-dl -x -i -o "%%(playlist)s/%%(title)s.%%(ext)s" --audio-format mp3 --audio-quality 3 %music%
echo /////////////////////////////////////////////////
echo Tudo pronto! Bom proveito!
echo Pressione qualquer tecla para fechar.
pause > nul
exit
